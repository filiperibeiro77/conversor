package conversor;
import java.io.*;

public class Main {

	public static void main(String[] args) throws IOException{
		
		InputStream entradaSistema = System.in;
		InputStreamReader leitor = new InputStreamReader(entradaSistema);
		BufferedReader leitorEntrada = new BufferedReader(leitor);
		String entradaTeclado;
		
		double umaTemperatura;
		char menu;
		
		Conversor umConversor = new Conversor();
		
		do{
			System.out.println("---Menu---");
			System.out.println("1 - Inserir uma temperatura Kelvin: ");
			System.out.println("2 - Inserir uma temperatura Celsius: ");
			System.out.println("3 - Inserir uma temperatura Fahrenheint: ");
			System.out.println("0 - Sair");
			System.out.println("Insira a a opção desejada: ");
			
			entradaTeclado = leitorEntrada.readLine();
			menu = entradaTeclado.charAt(0);
			
			while (menu != '0' && menu != '1' && menu != '2' && menu != '3'){
					System.out.println("O valor inserido é invalido =( Tente novamente: ");	
					entradaTeclado = leitorEntrada.readLine();
					menu = entradaTeclado.charAt(0);
	        }
			
			if(menu == '1'){
				System.out.println("Insira a temperatura em Kelvin para ser convertida: ");
				entradaTeclado = leitorEntrada.readLine();
				umaTemperatura = Double.parseDouble(entradaTeclado);
				double resultado1 = umConversor.converteKelvinCelsius(umaTemperatura);
				double resultado2 = umConversor.converteKelvinFahrenheint(umaTemperatura);
				
				System.out.println("Essa temperatura em celisus é: Cº " +resultado1);
				System.out.println("Essa temperatura em fahrenheint é: Fº " +resultado2);
			}
			
			else if(menu == '2'){
				System.out.println("Insira a temperatura em Celsius para ser convertida: ");
				entradaTeclado = leitorEntrada.readLine();
				umaTemperatura = Double.parseDouble(entradaTeclado);
				double resultado1 = umConversor.converteCelsiusKelvin(umaTemperatura);
				double resultado2 = umConversor.converteCelsiusFahrenheint(umaTemperatura);
				

				System.out.println("Essa temperatura em kelvin é: Kº " +resultado1);
				System.out.println("Essa temperatura em fahrenheint é: Fº " +resultado2);
			}
			
			else if(menu == '3'){
				System.out.println("Insira a temperatura em Fahrenheint para ser convertida: ");
				entradaTeclado = leitorEntrada.readLine();
				umaTemperatura = Double.parseDouble(entradaTeclado);
				double resultado1 = umConversor.converteFahrenheintCelsius(umaTemperatura);
				double resultado2 = umConversor.converteFahrenheintKelvin(umaTemperatura);
				
				System.out.println("Essa temperatura em celsius é: Cº " +resultado1);
				System.out.println("Essa temperatura em kelvin é: Kº " +resultado2);
			}
			
		}while(menu != '0');

	}

}
