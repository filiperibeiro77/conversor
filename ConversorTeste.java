package conversor;

import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.Before;

public class ConversorTeste {
	private Conversor umConversor;

	@Before
    public void setUp() throws Exception {
		umConversor = new Conversor();
	}
	
	@Test
	public void testKelvinCelsius(){
		assertEquals(-218.0, umConversor.converteKelvinCelsius(55), 1);
	}
	
	@Test
	public void testKelvinFahrenheint(){
		assertEquals(-360.4, umConversor.converteKelvinFahrenheint(55), 1);
	}
	
	@Test
	public void testCelsiusKelvin(){
		assertEquals(350.0, umConversor.converteCelsiusKelvin(77), 1);
	}
	
	@Test
	public void testCelsiusFahrenheint(){
		assertEquals(170.6, umConversor.converteCelsiusFahrenheint(77), 1);
	}
	
	@Test
	public void testFahrenheintCelsius(){
		assertEquals(96.11111111111111, umConversor.converteFahrenheintCelsius(205), 1);
	}
	
	@Test 
	public void testFahrenheintKelvin(){
		assertEquals(369.1111111111111, umConversor.converteFahrenheintKelvin(205), 1);
	}

}
