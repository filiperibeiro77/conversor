package conversor;

public class Conversor {
	private double fahrenheint;
	private double celsius;
	private double kelvin;
	
	public Conversor(){
		
	}
	
	public Conversor(double tempCelsius, double tempKelvin, double tempFahrenheint){
		this.celsius = tempCelsius;
		this.kelvin = tempKelvin;
		this.fahrenheint = tempFahrenheint;
	}
	
	public double getFahrenheint() {
		return fahrenheint;
	}
	public void setFahrenheint(double fahrenheint) {
		this.fahrenheint = fahrenheint;
	}
	public double getCelsius() {
		return celsius;
	}
	public void setCelsius(double celsius) {
		this.celsius = celsius;
	}
	public double getKelvin() {
		return kelvin;
	}
	public void setKelvin(double kelvin) {
		this.kelvin = kelvin;
	}
	
	public double converteCelsiusKelvin(double tempCelsius){
		return tempCelsius + 273;
	}
	
	public double converteCelsiusFahrenheint(double tempCelsius){
		return (tempCelsius*9)/5 + 32;
	}
	
	public double converteKelvinCelsius(double tempKelvin){
		return tempKelvin - 273;
	}
	
	public double converteKelvinFahrenheint(double tempKelvin){
		return (converteKelvinCelsius(tempKelvin)*9)/5 + 32;
	}
	
	public double converteFahrenheintCelsius(double tempFahrenheint){
		return ((tempFahrenheint - 32)*5)/9;
	}
	
	public double converteFahrenheintKelvin(double tempFahrenheint){
		return 	((tempFahrenheint - 32)*5)/9 + 273;
	}
}